<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'first_name'=>'Admin',
            'email'=>'admin@admin.com',
            'password'=>'123456',
            'mobile_number'=>'123456789',
            'role_id'=>1,
            'last_name'=>'Admin',
            'address'=>'Admin Address',
            'city'=>'Admin City',
            'gender'=>'Male',
            'is_activated'=>true,
            'account_status_id'=>2

        ]);
    }
}
