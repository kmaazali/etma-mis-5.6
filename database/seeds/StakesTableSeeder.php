<?php

use Illuminate\Database\Seeder;
use App\Stakes;

class StakesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Stakes::create([
            'stakes_name'=>'Jasper',
            'stakes_to'=>'1',
            'stakes_from'=>'100000'
        ]);
        Stakes::create([
            'stakes_name'=>'Emerald',
            'stakes_to'=>'100000',
            'stakes_from'=>'500000'
        ]);
        Stakes::create([
            'stakes_name'=>'Ruby',
            'stakes_to'=>'500000',
            'stakes_from'=>'1000000'
        ]);
        Stakes::create([
            'stakes_name'=>'Diamond',
            'stakes_to'=>'1000000',
            'stakes_from'=>'2000000'
        ]);
        Stakes::create([
            'stakes_name'=>'Diamond',
            'stakes_to'=>'1000000',
            'stakes_from'=>'2000000'
        ]);
        Stakes::create([
            'stakes_name'=>'Silver',
            'stakes_to'=>'2,000,000',
            'stakes_from'=>'5000000'
        ]);
        Stakes::create([
            'stakes_name'=>'Gold',
            'stakes_to'=>'5000000',
            'stakes_from'=>'10000000'
        ]);
        Stakes::create([
            'stakes_name'=>'Platinum',
            'stakes_to'=>'10000000',

        ]);

    }
}
