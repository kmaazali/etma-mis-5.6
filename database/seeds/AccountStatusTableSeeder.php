<?php

use Illuminate\Database\Seeder;

class AccountStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\AccountStatus::create(
            ['status'=>'Pending']
        );
        \App\AccountStatus::create([
            'status'=>'Verified'
        ]);
        \App\AccountStatus::create([
            'status'=>'Blocked'
        ]);
    }
}
