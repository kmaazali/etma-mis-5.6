<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('me', 'App\\Api\\V1\\Controllers\\UserController@me');

    });


    //Admin Panel Routes
    Route::group(['middleware'=>['auth:api','admin']], function($router) {
        Route::get('get/donations','DonorController@getAllDonations');
    });


    //Donor Panel Routes
    Route::group(['middleware'=>['auth:api','donor']], function($router) {
        //Stripe Payment Route
        Route::post('pay/stripe','DonorController@stripePayment');
        Route::get('get/my/donations','DonorController@getDonations');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
    $api->post('donor/signup/one','App\Http\Controllers\DonorController@DonorSignupOne');
    $api->post('donor/signup/two','App\Http\Controllers\DonorController@DonorSignupTwo');


    //Get Objects
    $api->get('get/stakes','App\Http\Controllers\DonorController@getStakes');
    $api->post('bcrypt','App\Http\Controllers\DonorController@bcryptgen');
});
