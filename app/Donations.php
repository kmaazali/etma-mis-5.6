<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donations extends Model
{
    //
    protected $table='donations';
    protected $fillable=['user_id','donation_amount','stakes_id'];


    public function stakes(){
        return $this->belongsTo('App\Stakes','stakes_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
