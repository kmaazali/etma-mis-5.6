<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerTokens extends Model
{
    //
    protected $table='customer_tokens';

    protected $fillable=['user_id','token'];
}
