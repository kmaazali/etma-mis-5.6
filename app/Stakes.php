<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stakes extends Model
{
    protected $table='stakes';

    protected $fillable=['stakes_name','stakes_to','stakes_from'];
    //
}
