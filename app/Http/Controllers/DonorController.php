<?php

namespace App\Http\Controllers;

use App\CustomerTokens;
use App\Donations;
use App\Stakes;
use App\User;
use Illuminate\Http\Request;
use AfricasTalkingGateway;

class DonorController extends Controller
{
    //

    function generateRandomString($length = 6) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function DonorSignupOne(Request $request){
        if($request->has('mobile_number')&&$request['mobile_number']!='') {
            $user_mobile=User::where('mobile_number',$request)->first();
            if($user_mobile->isEmpty()){
                $rand_code=$this->generateRandomString();
                require('AfricasTalkingGateway.php');
                $username = "etmamis";
                $apikey = "123bc4bb564ede2608e2b9dc82eff177c47e991a1074e6fffe07caedbd57c7aa";
// Specify the numbers that you want to send to in a comma-separated list
// Please ensure you include the country code (+254 for Kenya in this case)
                $recipients = $request["mobile_number"];


// And of course we want our recipients to know what we really do
                $message = "Your OTP Code For ETMA-MIS is ".$rand_code."";
// Create a new instance of our awesome gateway class
                $gateway = new AfricasTalkingGateway($username, $apikey);
                /*************************************************************************************
                 * NOTE: If connecting to the sandbox:
                 * 1. Use "sandbox" as the username
                 * 2. Use the apiKey generated from your sandbox application
                 * https://account.africastalking.com/apps/sandbox/settings/key
                 * 3. Add the "sandbox" flag to the constructor
                 * $gateway  = new AfricasTalkingGateway($username, $apiKey, "sandbox");
                 **************************************************************************************/
// Any gateway error will be captured by our custom Exception class below,
// so wrap the call in a try-catch block
                try {
                    // Thats it, hit send and we'll take care of the rest.
                    $results = $gateway->sendMessage($recipients, $message);
                    User::create([
                        'mobile_number'=>$request['mobile_number'],
                        'activation_code'=>$rand_code,
                        'is_activated'=>false,
                        'role_id'=>2
                    ]);
                    return response()->json(['success'=>true,'message'=>'Kindly Use The OTP On Next Page To Complete Signup','code'=>$rand_code],200);

                    //dd($results);
                } catch (AfricasTalkingGatewayException $e) {

                    echo "Encountered an error while sending: " . $e->getMessage();
                }
            }
            else{
                return response()->json(['success'=>false,'message'=>'Mobile Number Already Exists. Please Select A Different Mobile Number'],406);
            }

        }
// DONE!!!
    }

    public function DonorSignupTwo(Request $request){
        if($request->has('email')&&$request->has('password')&&$request->has('address')){
            $user=User::where('mobile_number',$request['mobile_number'])->where('activation_code',$request['code'])->first();
            $user->email=$request['email'];
            $user->password=bcrypt($request['password']);
            $user->first_name=$request['first_name'];
            $user->last_name=$request['last_name'];
            $user->address=$request['address'];
            $user->city=$request['city'];
            $user->gender=$request['gender'];
            $user->notes=$request['notes'];
            $user->activation_code=$request['code'];
            $user->account_status_id=2;
            $user->save();
            return response()->json(['success'=>true,'message'=>'User Successfully Signed Up.'],200);
        }
        else{
            return response()->json(['success'=>false,'message'=>'Please Fill The Required Fields'],406);
        }
    }

    public function donorSignupThree(Request $request){
        if($request->has('stake_id')){

        }
        else if($request->has('donation_amount')){

        }
        else{
            return response()->json(['success'=>false,'message'=>'You Have To Select A Valid Amount Stake First'],406);
        }
    }


    public function stripePayment(Request $request){
        $user=auth()->user()->id;
        //$cust_token=CustomerTokens::where('user_id',$user)->get();

            //dd($cust_token);
            \Stripe\Stripe::setApiKey("sk_test_mpgoRoH4WzD4hTgp9ITTCKbb");

            $token=\Stripe\Token::create(array(
                "card" => array(
                    "number" => $request['card_number'],
                    "exp_month" => $request['exp_month'],
                    "exp_year" => $request['exp_year'],
                    "cvc" => $request['cvc']
                )
            ));
            CustomerTokens::create([
                'token'=>$token->id,
                'user_id'=>$user
            ]);
            \Stripe\Stripe::setApiKey("sk_test_mpgoRoH4WzD4hTgp9ITTCKbb");

            $charge=\Stripe\Charge::create(array(
                "amount" => $request['amount']*100,
                "currency" => "usd",
                "source" => $token["id"], // obtained with Stripe.js
                "description" => "Charge for joshua.jackson@example.com"
            ));

            $donation=Donations::create([
                'user_id'=>$user,
                'donation_amount'=>$request["amount"]
            ]);
            return response()->json(['success'=>true,'message'=>'Donation Submitted Successfully'],200);

    }

    //get stakes
    public function getStakes(){
        $stakes=Stakes::get();
        return response()->json(['success'=>true,'stakes'=>$stakes],200);
    }
    public function getAllDonations(){
        $donations=Donations::with('user')->get();
        return response()->json(['success'=>true,'donations'=>$donations],200);
    }

    public function getDonations(){
        $user_id=auth()->user()->id;
        $donations=Donations::where('user_id',$user_id)->with('user')->get();

    }

    public function bcryptgen(Request $request){
        $encrypted=bcrypt($request["key"]);
        return $encrypted;
    }

}
