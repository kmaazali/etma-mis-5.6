<?php

namespace App\Http\Middleware;

use Closure;

class DonorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->role_id != 2)
        {
            return response()->json(['success'=>false,'message'=>'Unauthorized'],401);
        }
        return $next($request);
    }
}
